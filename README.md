# JDBC Assignment (20%)  
## Internet of Things Device Manager  

> Module: Data Architecture & Database Systems  
> Author: Rebecca Kane  

### Overview

As the number of connected devices we use every day continues to grow at a rapid pace, one challenge we face is the management of these devices.   

This project provices a simple, small scale solution to this problem, allowing a user to keep track of the various devices throughout their home.  

Features of the system include:  

*  Add devices to the database  
*  Update or delete existing devices  
*  View a list of stored devices, including device name, type, location within the home, IP, MAC, software version and date last updated  
*  Add rooms to the system, as well as update and delete existing rooms  
*  Export to csv file: all devices or a specific set of devices such as devices of a specific type, devices in a specific location, or devices not updated since a specific date  
*  View pie charts: number of devices by type, or number of devices by room  