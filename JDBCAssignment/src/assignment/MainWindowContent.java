package assignment;

import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.PrintWriter;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;

import org.jdatepicker.JDatePanel;
import org.jdatepicker.JDatePicker;
import org.jdatepicker.UtilDateModel;
import org.jdatepicker.constraints.RangeConstraint;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.general.DefaultPieDataset;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;

@SuppressWarnings("serial")
public class MainWindowContent extends JInternalFrame implements ActionListener {
	// DB Connectivity Attributes
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	// Panels
	private Container content;
	private JPanel detailsPanel;
	private JPanel newRoomPanel;
	private JScrollPane roomsTableContentsPanel;
	private JPanel performActionOnSelectedRoomPanel;
	private JScrollPane devicesTableContentsPanel;
	private JPanel performActionOnSelectedDevicePanel;
	private Border lineBorder;

	// Add Device labels / fields
	private JLabel deviceNameLabel = new JLabel("Device Name: ");
	private JLabel deviceTypeLabel = new JLabel("Type: ");
	private JLabel deviceLocationLabel = new JLabel("Location: ");
	private JLabel deviceIPLabel = new JLabel("IP: ");
	private JLabel deviceMACLabel = new JLabel("MAC: ");
	private JLabel deviceSoftwareVersion = new JLabel("Current Software Version: ");
	private JLabel dateLastUpdatedLabel = new JLabel("Date Last Updated: ");

	private JTextField nameTF = new JTextField(10);
	String[] deviceTypes = { "SmartHome", "Console", "Kitchen Appliance", "SmartTV", "Smartphone", "Smartwatch", "PC", "Tablet",
			"Other" };
	private JComboBox<String> typeDD = new JComboBox<>(deviceTypes);
	ArrayList<String> locationsList;
	private JComboBox<String> locationDD = new JComboBox<>();
	private JTextField ipTF = new JTextField(10);
	private JTextField macTF = new JTextField(10);
	private JTextField softVerTF = new JTextField(10);
	UtilDateModel model = new UtilDateModel();
	JDatePanel datePanel = new JDatePanel(model);
	JDatePicker datePicker = new JDatePicker();

	// Insert / Clear Device buttons
	private JButton insertButton = new JButton("Add Device");
	private JButton clearButton = new JButton("Clear Fields");

	// Update / Delete Device buttons
	private String selectedDevice;
	private JButton updateDevBtn = new JButton("Update");
	private JButton deleteDevBtn = new JButton("Delete");

	// Add Room label/field
	private JLabel newRoomNameLabel = new JLabel("Room Name: ");
	private JTextField newRoomNameTF = new JTextField(15);
	private JButton addNewRoomButton = new JButton("Add Room");

	// Query Models
	private static QueryTableModel devicesTableModel = new QueryTableModel();
	private static QueryTableModel roomsTableModel = new QueryTableModel();
	static JTable tableOfDevices = new JTable(devicesTableModel); // Add the models to JTables, devices static because
																	// widths being set in QTM class, rooms doesn't need
																	// to be static
	private JTable tableOfRooms = new JTable(roomsTableModel);

	// Update / Delete Rooms buttons
	private String selectedRoomName = "";
	private JButton updateRoomBtn = new JButton("Update");
	private JButton deleteRoomBtn = new JButton("Delete");

	// Export Buttons
	private JLabel totalNumberOfDevices = new JLabel("Total Devices: ");
	private JButton viewNumbersByType = new JButton("View Numbers by Type");
	private JButton saveNumbersByType = new JButton();
	private JButton viewNumbersByLocation = new JButton("View Numbers by Location");
	private JButton saveNumbersByLocation = new JButton();
	
	private JLabel exportDevicesOfTypeLabel = new JLabel("Export Devices of Type: ");
	private JComboBox<String> exportTypeDD = new JComboBox<>(deviceTypes);
	private JButton exportDevicesOfTypeBtn = new JButton("Export");

	private JLabel exportDevicesInLocationLabel = new JLabel("Export Devices in Location:");
	private JComboBox<String> exportLocationDD = new JComboBox<>();
	private JButton exportDevicesInLocationBtn = new JButton("Export");

	private JLabel exportDevicesNotUpdatedSinceLabel = new JLabel("Export Devices Not Updated Since:");
	JDatePanel exportDatePanel = new JDatePanel(model);
	JDatePicker exportDatePicker = new JDatePicker();
	private JButton exportDevicesNotUpdatedSinceBtn = new JButton("Export");
	
	private JButton exportAllDataBtn = new JButton("Export All Device Data");
	private JButton viewDeviceTypeInfoBtn = new JButton("View Table of Device Types");

	// Colours
	public static final Color LIGHT_BLUE = new Color(230, 239, 255);
	public static final Color MED_BLUE = new Color(153, 190, 255);
	public static final Color DARKER_BLUE = new Color(64, 78, 102);

	// Current date (in ints and a Calendar object)
	int year = Calendar.getInstance().get(Calendar.YEAR);
	int month = Calendar.getInstance().get(Calendar.MONTH);
	int day = Calendar.getInstance().get(Calendar.DATE);
	Calendar currentDate = Calendar.getInstance();
	String currentDateString = day + String.format("-%02d-", month + 1) + year;

	public MainWindowContent(String aTitle) {
		// setting up the GUI
		super(aTitle, false, false, false, false);
		setEnabled(true);

		initiate_db_conn();
		// add the 'main' panel to the Internal Frame
		content = getContentPane();
		content.setLayout(null);
		content.setBackground(LIGHT_BLUE);
		lineBorder = BorderFactory.createLineBorder(MED_BLUE, 1);

		// Device Information
		detailsPanel = new JPanel();
		detailsPanel.setLayout(new GridLayout(14, 1));
		detailsPanel.setBackground(LIGHT_BLUE);
		detailsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Add / Update Device"));

		detailsPanel.add(deviceNameLabel);
		detailsPanel.add(nameTF);
		detailsPanel.add(deviceTypeLabel);
		typeDD.setSelectedItem(0);
		typeDD.addActionListener(this);
		detailsPanel.add(typeDD);
		detailsPanel.add(deviceLocationLabel);
		locationDD.setSelectedItem(0);
		locationDD.addActionListener(this);
		detailsPanel.add(locationDD);
		detailsPanel.add(deviceIPLabel);
		detailsPanel.add(ipTF);
		detailsPanel.add(deviceMACLabel);
		detailsPanel.add(macTF);
		detailsPanel.add(deviceSoftwareVersion);
		detailsPanel.add(softVerTF);
		detailsPanel.add(dateLastUpdatedLabel);
		Calendar myCal = Calendar.getInstance(); // Create a calendar object for "after" param for range constraint
		myCal.set(1999, 01, 01);
		RangeConstraint constraint = new RangeConstraint(myCal.getTime(), Calendar.getInstance().getTime()); // Add a constraint to the date picker, can't allow user pick future date
		datePicker.addDateSelectionConstraint(constraint);

		datePicker.getModel().setDay(day);
		datePicker.getModel().setMonth(month);
		datePicker.getModel().setYear(year);
		datePicker.getModel().setSelected(true);

		detailsPanel.add(datePicker);
		detailsPanel.setSize(300, 350);
		detailsPanel.setLocation(20, 10);
		content.add(detailsPanel);

		// Insert / Clear Buttons
		insertButton.setSize(130, 30);
		clearButton.setSize(130, 30);
		insertButton.setLocation(30, 365);
		clearButton.setLocation(175, 365);
		insertButton.addActionListener(this);
		clearButton.addActionListener(this);
		content.add(insertButton);
		content.add(clearButton);

		// Update / Delete Device Buttons
		performActionOnSelectedDevicePanel = new JPanel();
		performActionOnSelectedDevicePanel.setBackground(LIGHT_BLUE);
		performActionOnSelectedDevicePanel.setLayout(new GridLayout(1, 2));
		performActionOnSelectedDevicePanel
				.setBorder(BorderFactory.createTitledBorder(lineBorder, "Perform Action on Selected Device"));
		updateDevBtn.addActionListener(this);
		deleteDevBtn.addActionListener(this);
		updateDevBtn.setLocation(1, 1);
		deleteDevBtn.setLocation(1, 2);
		performActionOnSelectedDevicePanel.add(updateDevBtn);
		performActionOnSelectedDevicePanel.add(deleteDevBtn);
		performActionOnSelectedDevicePanel.setSize(300, 50);
		performActionOnSelectedDevicePanel.setLocation(20, 405);
		content.add(performActionOnSelectedDevicePanel);

		// Table of Rooms
		tableOfRooms.setPreferredScrollableViewportSize(new Dimension(300, 140));
		roomsTableContentsPanel = new JScrollPane(tableOfRooms, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		roomsTableContentsPanel.setBackground(LIGHT_BLUE);
		roomsTableContentsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Rooms in System"));
		roomsTableContentsPanel.setSize(300, 140);
		roomsTableContentsPanel.setLocation(20, 485);
		content.add(roomsTableContentsPanel);
		roomsTableModel.refreshRoomsTable(stmt);

		// Add / Delete a Room
		newRoomPanel = new JPanel();
		newRoomPanel.setLayout(new GridLayout(3, 1));
		newRoomPanel.setBackground(LIGHT_BLUE);
		newRoomPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Add / Update Room"));
		newRoomPanel.add(newRoomNameLabel);
		newRoomPanel.add(newRoomNameTF);
		addNewRoomButton.setSize(100, 30);
		addNewRoomButton.addActionListener(this);
		newRoomPanel.add(addNewRoomButton);
		newRoomPanel.setSize(300, 100);
		newRoomPanel.setLocation(20, 630);
		content.add(newRoomPanel);

		// Update / Delete Room Buttons
		performActionOnSelectedRoomPanel = new JPanel();
		performActionOnSelectedRoomPanel.setBackground(LIGHT_BLUE);
		performActionOnSelectedRoomPanel.setLayout(new GridLayout(1, 2));
		performActionOnSelectedRoomPanel
				.setBorder(BorderFactory.createTitledBorder(lineBorder, "Perform Action on Selected Room"));
		updateRoomBtn.addActionListener(this);
		deleteRoomBtn.addActionListener(this);
		updateRoomBtn.setLocation(1, 1);
		deleteRoomBtn.setLocation(1, 2);
		performActionOnSelectedRoomPanel.add(updateRoomBtn);
		performActionOnSelectedRoomPanel.add(deleteRoomBtn);
		performActionOnSelectedRoomPanel.setSize(300, 50);
		performActionOnSelectedRoomPanel.setLocation(20, 735);
		content.add(performActionOnSelectedRoomPanel);

		// Table of Devices
		tableOfDevices.setPreferredScrollableViewportSize(new Dimension(750, 520));
		devicesTableContentsPanel = new JScrollPane(tableOfDevices, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		devicesTableContentsPanel.setBackground(LIGHT_BLUE);
		devicesTableContentsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Devices in System"));
		devicesTableContentsPanel.setSize(750, 520);
		devicesTableContentsPanel.setLocation(390, 10);

		content.add(devicesTableContentsPanel);
		devicesTableModel.refreshDevicesTable(stmt);
		tableOfDevices.getColumnModel().getColumn(0).setCellRenderer(new WordWrapCellRenderer()); // Wrapping on device name
		
		// Export Buttons
		totalNumberOfDevices.setLocation(450, 525);
		totalNumberOfDevices.setSize(150, 50);
		totalNumberOfDevices.setFont(new Font("Dialog", Font.BOLD, 14));
		totalNumberOfDevices.setBackground(LIGHT_BLUE);
		content.add(totalNumberOfDevices);
		
		viewNumbersByType.setLocation(450, 570);
		viewNumbersByType.setSize(300, 30);
		viewNumbersByType.addActionListener(this);
		content.add(viewNumbersByType);
		viewNumbersByLocation.setLocation(450, 605);
		viewNumbersByLocation.setSize(300, 30);
		viewNumbersByLocation.addActionListener(this);
		content.add(viewNumbersByLocation);
		
		exportAllDataBtn.setSize(300, 30);
		exportAllDataBtn.setLocation(770, 570);
		exportAllDataBtn.addActionListener(this);
		content.add(exportAllDataBtn);
		viewDeviceTypeInfoBtn.setSize(300, 30);
		viewDeviceTypeInfoBtn.setLocation(770, 605);
		viewDeviceTypeInfoBtn.addActionListener(this);
		content.add(viewDeviceTypeInfoBtn);
		
		exportDevicesOfTypeLabel.setLocation(450, 650);
		exportDevicesOfTypeLabel.setSize(200, 30);
		exportDevicesOfTypeLabel.setBackground(LIGHT_BLUE);
		content.add(exportDevicesOfTypeLabel);
		exportTypeDD.setLocation(700, 650);
		exportTypeDD.setSize(150, 30);
		content.add(exportTypeDD);
		exportDevicesOfTypeBtn.setLocation(920, 650);
		exportDevicesOfTypeBtn.setSize(150, 30);
		exportDevicesOfTypeBtn.addActionListener(this);
		content.add(exportDevicesOfTypeBtn);
		
		exportDevicesInLocationLabel.setLocation(450, 690);
		exportDevicesInLocationLabel.setSize(200, 30);
		content.add(exportDevicesInLocationLabel);
		exportLocationDD.setSelectedItem(0);
		exportLocationDD.addActionListener(this);
		exportLocationDD.setLocation(700, 690);
		exportLocationDD.setSize(150, 30);
		content.add(exportLocationDD);
		exportDevicesInLocationBtn.setLocation(920, 690);
		exportDevicesInLocationBtn.setSize(150, 30);
		exportDevicesInLocationBtn.addActionListener(this);
		content.add(exportDevicesInLocationBtn);
		
		exportDevicesNotUpdatedSinceLabel.setLocation(450, 730);
		exportDevicesNotUpdatedSinceLabel.setSize(200, 30);
		content.add(exportDevicesNotUpdatedSinceLabel);
		exportDatePicker.addDateSelectionConstraint(constraint);
		exportDatePicker.getModel().setDay(day);
		exportDatePicker.getModel().setMonth(month);
		exportDatePicker.getModel().setYear(year);
		exportDatePicker.getModel().setSelected(true);
		exportDatePicker.setLocation(700, 730);
		exportDatePicker.setSize(150, 30);
		content.add(exportDatePicker);
		exportDevicesNotUpdatedSinceBtn.setLocation(920, 730);
		exportDevicesNotUpdatedSinceBtn.setSize(150, 30);
		exportDevicesNotUpdatedSinceBtn.addActionListener(this);
		content.add(exportDevicesNotUpdatedSinceBtn);

		setSize(1200, 900);
		setVisible(true);

		// Adapted from https://stackoverflow.com/a/32942079 - get selected row
		tableOfDevices.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {
				selectedDevice = tableOfDevices.getValueAt(tableOfDevices.getSelectedRow(), 0).toString();

				nameTF.setText(tableOfDevices.getValueAt(tableOfDevices.getSelectedRow(), 0).toString());
				typeDD.setSelectedItem(tableOfDevices.getValueAt(tableOfDevices.getSelectedRow(), 1).toString());
				locationDD.setSelectedItem(tableOfDevices.getValueAt(tableOfDevices.getSelectedRow(), 2).toString());
				ipTF.setText(tableOfDevices.getValueAt(tableOfDevices.getSelectedRow(), 3).toString());
				macTF.setText(tableOfDevices.getValueAt(tableOfDevices.getSelectedRow(), 4).toString());
				softVerTF.setText(tableOfDevices.getValueAt(tableOfDevices.getSelectedRow(), 5).toString());

				String[] fullDate = tableOfDevices.getValueAt(tableOfDevices.getSelectedRow(), 6).toString().split("-");
				datePicker.getModel().setYear(Integer.parseInt(fullDate[0]));
				datePicker.getModel().setMonth(Integer.parseInt(fullDate[1]) - 1); // Months start from 0 in the date
																					// picker for some reason (day/year
																					// don't)
				datePicker.getModel().setDay(Integer.parseInt(fullDate[2]));
			}
		});

		tableOfRooms.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {
				selectedRoomName = tableOfRooms.getValueAt(tableOfRooms.getSelectedRow(), 0).toString();

				newRoomNameTF.setText(selectedRoomName);
			}
		});
	}

	public void initiate_db_conn() {
		try {
			Class.forName("com.mysql.jdbc.Driver"); // Load the JConnector Driver
			String url = "jdbc:mysql://localhost:3306/IoT_Device_Manager"; // Specify the DB Name
			con = DriverManager.getConnection(url, "root", "admin"); // Connect to DB using DB URL, Username and
																		// password
			stmt = con.createStatement(); // Create a generic statement
			refreshRoomsDropDown(); // Populate rooms drop down in device details section
			refreshNumberOfDevices(); // Display how many devices are in the db
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
			JOptionPane.showMessageDialog(new JFrame(),
					"Problem encountered while connecting to the database.\nPlease try again.");
		}
	}

	public void refreshRoomsDropDown() {
		ResultSet locations;
		try {
			locations = stmt.executeQuery("Select roomName from locations");
			locationsList = new ArrayList<String>();
			while (locations.next()) {
				locationsList.add(locations.getString(1));
			}

			locationDD.setModel(new DefaultComboBoxModel(locationsList.toArray()));
			exportLocationDD.setModel(new DefaultComboBoxModel(locationsList.toArray()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void refreshNumberOfDevices() {
		try {
			rs = stmt.executeQuery("select count(*) from devices");
			while (rs.next()) {
				totalNumberOfDevices.setText("Total Devices: " + rs.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void clearDeviceFields() {
		nameTF.setText("");
		typeDD.setSelectedItem(0);
		locationDD.setSelectedItem(0);
		ipTF.setText("");
		macTF.setText("");
		softVerTF.setText("");
		datePicker.getModel().setDay(day);
		datePicker.getModel().setMonth(month);
		datePicker.getModel().setYear(year);
		datePicker.getModel().setSelected(true);

	}

	public void exportToCSVFile(ResultSet rs, String filename) {
		try {
			FileWriter outputFile = new FileWriter(filename + ".csv");
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for (int i = 0; i < numColumns; i++) {
				printWriter.print(rsmd.getColumnLabel(i + 1) + ",");
			}
			printWriter.print("\n");
			while (rs.next()) {
				for (int i = 0; i < numColumns; i++) {
					printWriter.print(rs.getString(i + 1) + ",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(new JFrame(),
					"Problem encountered while writing to file.\nPlease try again.");
		}
	}
	
	public  void pieGraph(ResultSet rs, String title) {
		try {
			DefaultPieDataset dataset = new DefaultPieDataset();

			while (rs.next()) {
				String category = rs.getString(1);
				String value = rs.getString(2);
				dataset.setValue(category+ ":\n"+value, new Double(value));
			}
			JFreeChart chart = ChartFactory.createPieChart3D(
					title,  
					dataset,             
					false,              
					true,
					true
			);
			chart.setBackgroundPaint(Color.WHITE);
			ChartFrame frame = new ChartFrame(title, chart);
			
			if (title.equals("Device Numbers By Type")) {
				saveNumbersByType = new JButton("Save to DeviceNumbersByType" + currentDateString + ".csv");
				saveNumbersByType.setSize(300, 30);
				saveNumbersByType.addActionListener(this);
				frame.add(saveNumbersByType);
			} else {
				saveNumbersByLocation = new JButton("Save to DeviceNumbersByLocation" + currentDateString + ".csv");
				saveNumbersByLocation.setSize(300, 30);
				saveNumbersByLocation.addActionListener(this);
				frame.add(saveNumbersByLocation);
			}
			
			frame.pack();
			frame.setVisible(true);
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void clearDB() {
		int choice = JOptionPane.showConfirmDialog(new JFrame(), "Are you sure you want to delete all devices from the database?\nThis cannot be undone.", "Warning", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);

		if (choice == 0) {
			try {
				stmt.executeUpdate("TRUNCATE TABLE devices;");
				//devicesTableModel.refreshDevicesTable(stmt);
				JOptionPane.showMessageDialog(new JFrame(), "Devices successfully deleted.");
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(new JFrame(), "Problem encountered while clearing devices database. \nPlease try again.");
			} finally {
				devicesTableModel.refreshDevicesTable(stmt);
				tableOfDevices.getColumnModel().getColumn(0).setCellRenderer(new WordWrapCellRenderer()); // Wrapping on device name
			}
		}
	}
	
	public void populateWithSampleData() {
		try {
			stmt.executeUpdate("call populateDatabase();");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(new JFrame(), "Problem encountered while populating devices database. \nPlease try again.");
		} finally {
			devicesTableModel.refreshDevicesTable(stmt);
			tableOfDevices.getColumnModel().getColumn(0).setCellRenderer(new WordWrapCellRenderer()); // Wrapping on device name
		}
	}

	// Event handlers
	public void actionPerformed(ActionEvent e) {
		Object target = e.getSource();

		// Add Clear Update Delete Buttons for Devices
		// ---------------------------------------------------------------------------------------------
		if (target == clearButton) {
			clearDeviceFields();
		}
		if (target == insertButton) {
			if (softVerTF.getText().equals("")) {
				JOptionPane.showMessageDialog(new JFrame(),
						"Name, type, location, software version and date are required fields.\nPlease try again.");
			} else {
				try {
					String dd, mm;
					int yyyy;
					// JDatePicker stores dates in Gregorian format, load of unnecessary stuff
					// Extracting the year, and day and month - padding them with leading 0s for cases < 10
					// Adding all together in a String in format yyyy-mm-dd, accepted by SQL as a Date
					// If else sets date to today if date after current date is selected
					// -> JDatePicker constraints can be bypassed by selecting valid date and using arrows to go forward
					if (currentDate.before(datePicker.getModel().getValue())) {
						dd = String.format("%02d", day);
						mm = String.format("%02d", month + 1);
						yyyy = year; // doesn't need to be padded
					} else {
						dd = String.format("%02d", datePicker.getModel().getDay());
						mm = String.format("%02d", datePicker.getModel().getMonth() + 1);
						yyyy = datePicker.getModel().getYear(); // doesn't need to be padded
					}
	
					String mysqlDate = yyyy + "-" + mm + "-" + dd;
	
					String insertTemp = "INSERT INTO devices VALUES ('" + nameTF.getText() + "','"
							+ typeDD.getSelectedItem().toString() + "','" + locationDD.getSelectedItem().toString() + "','"
							+ ipTF.getText() + "','" + macTF.getText() + "','" + softVerTF.getText() + "','" + mysqlDate
							+ "');";
	
					stmt.executeUpdate(insertTemp);
					JOptionPane.showMessageDialog(new JFrame(), nameTF.getText() + " added to system!");
					clearDeviceFields();
					tableOfDevices.getColumnModel().getColumn(0).setCellRenderer(new WordWrapCellRenderer()); // Wrapping on device name
					tableOfDevices.addNotify();
					refreshNumberOfDevices();
				} catch (SQLException sqle) {
					System.err.println("Error with members insert:\n" + sqle.toString());
					JOptionPane.showMessageDialog(new JFrame(),
							"Problem encountered while adding new device to system.\nPlease try again.");
	
				} finally {
					devicesTableModel.refreshDevicesTable(stmt);
					tableOfDevices.getColumnModel().getColumn(0).setCellRenderer(new WordWrapCellRenderer()); // Wrapping on device name
				}
			}
		}
		if (target == deleteDevBtn) {
			try {
				String deleteTemp = "DELETE FROM DEVICES WHERE deviceName = '" + nameTF.getText() + "';";
				stmt.executeUpdate(deleteTemp);
				JOptionPane.showMessageDialog(new JFrame(), "Deleted " + selectedDevice + " successfully.");
				tableOfDevices.addNotify(); // Reconfigures the displayed table
				selectedDevice = "";
				clearDeviceFields();
				refreshNumberOfDevices();
			} catch (SQLException sqle) {
				System.err.println("Error with delete:\n" + sqle.toString());
				JOptionPane.showMessageDialog(new JFrame(),
						"Problem encountered while deleting Device " + nameTF.getText() + ".\nPlease try again.");
			} finally {
				devicesTableModel.refreshDevicesTable(stmt);
				tableOfDevices.getColumnModel().getColumn(0).setCellRenderer(new WordWrapCellRenderer()); // Wrapping on device name
				tableOfDevices.clearSelection();
			}
		}
		if (target == updateDevBtn) {
			if (softVerTF.getText().equals("")) {
				JOptionPane.showMessageDialog(new JFrame(),
						"Name, type, location, software version and date are required fields.\nPlease try again.");
			} else {
				try {
					String dd, mm;
					int yyyy;
					if (currentDate.before(datePicker.getModel().getValue())) {
						dd = String.format("%02d", day);
						mm = String.format("%02d", month + 1);
						yyyy = year; // doesn't need to be padded
					} else {
						dd = String.format("%02d", datePicker.getModel().getDay());
						mm = String.format("%02d", datePicker.getModel().getMonth() + 1);
						yyyy = datePicker.getModel().getYear(); // doesn't need to be padded
					}
	
					String mysqlDate = yyyy + "-" + mm + "-" + dd;
	
					String updateTemp = "UPDATE devices SET " /* deviceID = '" + idTF.getText() */
							+ "deviceName = '" + nameTF.getText() + "', deviceType = '"
							+ typeDD.getSelectedItem().toString() + "', deviceLocation = '"
							+ locationDD.getSelectedItem().toString() + "', deviceIP = '" + ipTF.getText()
							+ "', deviceMAC = '" + macTF.getText() + "', softwareVersion = '" + softVerTF.getText()
							+ "', dateLastUpdated = '" + mysqlDate + "' where deviceName = '" + selectedDevice + "';";
					stmt.executeUpdate(updateTemp);
					JOptionPane.showMessageDialog(new JFrame(), "Updated " + nameTF.getText() + " successfully");
					clearDeviceFields();
					selectedDevice = "";
					tableOfDevices.addNotify(); // Reconfigures the displayed table
				} catch (SQLException sqle) {
					System.err.println("Error with members insert:\n" + sqle.toString());
					JOptionPane.showMessageDialog(new JFrame(),
							"Problem encountered while updating " + nameTF.getText() + ".\nPlease try again.");
				} finally {
					devicesTableModel.refreshDevicesTable(stmt);
					tableOfDevices.getColumnModel().getColumn(0).setCellRenderer(new WordWrapCellRenderer()); // Wrapping on device name
				}
			}
		}

		// Add Update Delete Buttons for Rooms
		// ---------------------------------------------------------------------------------------------
		if (target == addNewRoomButton) {
			if (newRoomNameTF.getText().equals("")) {
				JOptionPane.showMessageDialog(new JFrame(), "Room name cannot be blank. Please try again.");
			} else {
				try {
					String newRoomStr = "INSERT INTO locations VALUES ('" + newRoomNameTF.getText() + "');";
					stmt.executeUpdate(newRoomStr);
					refreshRoomsDropDown();
					JOptionPane.showMessageDialog(new JFrame(), newRoomNameTF.getText() + " added to system!");
					newRoomNameTF.setText("");
					tableOfRooms.addNotify();
				} catch (SQLException sqle) {
					System.err.println("Error with members insert:\n" + sqle.toString());
					JOptionPane.showMessageDialog(new JFrame(), "Problem encountered while adding "
							+ newRoomNameTF.getText() + " to system.\nPlease try again.");

				} finally {
					roomsTableModel.refreshRoomsTable(stmt);
				}
			}

		}
		if (target == deleteRoomBtn) {
			try {
				String deleteTemp = "DELETE FROM locations WHERE roomName = '" + selectedRoomName + "';";
				stmt.executeUpdate(deleteTemp);
				tableOfRooms.addNotify(); // Reconfigures the displayed table
				JOptionPane.showMessageDialog(new JFrame(), "Deleted " + selectedRoomName + " successfully.");
				newRoomNameTF.setText("");
				refreshRoomsDropDown();
				selectedRoomName = "";
			} catch (SQLException sqle) {
				System.err.println("Error with delete:\n" + sqle.toString());
				JOptionPane.showMessageDialog(new JFrame(), "Problem encountered while deleting room "
						+ selectedRoomName
						+ ".\nNote: Deletion isn't possible if there are existing devices in the system allocated to this room.");
			} finally {
				roomsTableModel.refreshRoomsTable(stmt);
				tableOfRooms.clearSelection();
			}
		}
		if (target == updateRoomBtn) {
			try {
				String updateTemp = "UPDATE locations SET roomName = '" + newRoomNameTF.getText()
						+ "' where roomName = '" + selectedRoomName + "';";
				stmt.executeUpdate(updateTemp);
				refreshRoomsDropDown();
				JOptionPane.showMessageDialog(new JFrame(),
						"Updated " + selectedRoomName + " successfully. \nNew name: " + newRoomNameTF.getText());
				newRoomNameTF.setText("");
				selectedRoomName = "";
			} catch (SQLException sqle) {
				System.err.println("Error with members update:\n" + sqle.toString());
				JOptionPane.showMessageDialog(new JFrame(), "Problem encountered while updating " + selectedRoomName
						+ ".\nNote: Updating isn't possible if there are existing devices in the system allocated to this room.");
			} finally {
				roomsTableModel.refreshRoomsTable(stmt);
				tableOfRooms.clearSelection();
			}
		}
		
		// Export / Analysis Section ------------------------------------------------------------------------
		// View Device Numbers by Type/Location buttons
		if (target == viewNumbersByType) {
			try {
				rs = stmt.executeQuery("select deviceType as Type, count(*) as Count from devices group by deviceType;");
				pieGraph(rs, "Device Numbers By Type");
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(new JFrame(), "Problem encountered while querying database.\nPlease try again. ");
			}
		}
		if (target == saveNumbersByType) {
			try {
				rs = stmt.executeQuery("select deviceType as Type, count(*) as Count from devices group by deviceType;");
				exportToCSVFile(rs, "DeviceNumbersByType" + currentDateString);
				JOptionPane.showMessageDialog(new JFrame(), "Saved DeviceNumbersByType" + currentDateString + ".csv successfully.");
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(new JFrame(), "Problem encountered while saving csv file.\nPlease try again.");
			}
		}
		if (target == viewNumbersByLocation) {
			try {
				rs = stmt.executeQuery("select deviceLocation as Location, count(*) as Count from devices group by deviceLocation;");
				pieGraph(rs, "Device Numbers By Location");
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(new JFrame(), "Problem encountered while querying database.\nPlease try again. ");
			}
		}
		if (target == saveNumbersByLocation) {
			try {
				rs = stmt.executeQuery("select deviceLocation as Location, count(*) as Count from devices group by deviceLocation;");
				exportToCSVFile(rs, "DeviceNumbersByLocation" + currentDateString);
				JOptionPane.showMessageDialog(new JFrame(), "Saved DeviceNumbersByLocation" + currentDateString + ".csv successfully.");
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(new JFrame(), "Problem encountered while saving csv file.\nPlease try again.");
			}
		}
		
		// Export Buttons ---------------------------------------------------------------------------------------------
		if (target == exportAllDataBtn) {
			try {
				rs = stmt.executeQuery("SELECT deviceName as Name, deviceType as Type, deviceLocation as Location, deviceIP as IP, deviceMAC as MAC, softwareVersion as Software, dateLastUpdated as LastUpdated FROM devices");
				String filename = "AllDevices" + currentDateString;
				exportToCSVFile(rs, filename);
				JOptionPane.showMessageDialog(new JFrame(),
						filename + " saved successfully.");
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(new JFrame(),
						"Problem encountered while querying database.\nPlease try again.");
				e1.printStackTrace();
			}
		}
		if (target == viewDeviceTypeInfoBtn) {
			try {
				rs = stmt.executeQuery("Select typeName as Name, typeDesc as Description from typesOfDevice");
				String[][] resultSet = new String[9][2];
				String[] colNames = { "Type", "Description" };
				int row = 0;
				while (rs.next()) {
					for (int i = 0; i < 2; i++) {
						resultSet[row][i] = rs.getObject(i + 1).toString() + "\n";
					}
					row++;
				}
				JTable tableOfDeviceTypes = new JTable(resultSet, colNames);
				tableOfDeviceTypes.getColumnModel().getColumn(0).setMinWidth(120);
				tableOfDeviceTypes.getColumnModel().getColumn(1).setMinWidth(280);
				tableOfDeviceTypes.setIntercellSpacing(new Dimension(10, 10));
				tableOfDeviceTypes.setRowHeight(40);
				tableOfDeviceTypes.getColumnModel().getColumn(0).setCellRenderer(new WordWrapCellRenderer());
				tableOfDeviceTypes.getColumnModel().getColumn(1).setCellRenderer(new WordWrapCellRenderer());
				JOptionPane.showMessageDialog(null, new JScrollPane(tableOfDeviceTypes));
			} catch (SQLException sqle) {
				sqle.printStackTrace();
				JOptionPane.showMessageDialog(new JFrame(),
						"Problem encountered with table: typesOfDevice.\nPlease try again.");

			}

		}
		if (target == exportDevicesOfTypeBtn) {
			try {
				rs = stmt.executeQuery("CALL exportDevicesOfType('" + exportTypeDD.getSelectedItem().toString() + "');");
				String filename = exportTypeDD.getSelectedItem().toString().replaceAll(" ", "") + "Devices" + currentDateString;
				exportToCSVFile(rs, filename);
				JOptionPane.showMessageDialog(new JFrame(), filename + " saved successfully.");
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(new JFrame(),
						"Problem encountered while querying database.\nPlease try again.");
				e1.printStackTrace();
			}
		}
		if (target == exportDevicesInLocationBtn) {
			try {
				rs = stmt.executeQuery("call exportDevicesInLocation('" + exportLocationDD.getSelectedItem().toString() + "');");
				String filename = exportLocationDD.getSelectedItem().toString().replaceAll(" ", "") + "Devices" + currentDateString;
				exportToCSVFile(rs, filename);
				JOptionPane.showMessageDialog(new JFrame(), filename + " saved successfully.");
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(new JFrame(),
						"Problem encountered while querying database.\nPlease try again.");
				e1.printStackTrace();
			}
		}
		if (target == exportDevicesNotUpdatedSinceBtn) {
			try {
				String dd, mm;
				int yyyy;
				if (currentDate.before(exportDatePicker.getModel().getValue())) {
					dd = String.format("%02d", day);
					mm = String.format("%02d", month + 1);
					yyyy = year; // doesn't need to be padded
				} else {
					dd = String.format("%02d", exportDatePicker.getModel().getDay());
					mm = String.format("%02d", exportDatePicker.getModel().getMonth() + 1);
					yyyy = exportDatePicker.getModel().getYear(); // doesn't need to be padded
				}

				String mysqlDate = yyyy + "-" + mm + "-" + dd;
				rs = stmt.executeQuery("call exportDevicesNotUpdatedSince('" + mysqlDate + "');");
				String filename ="DevicesNotUpdatedSince" + dd + "-" + mm + "-" + yyyy;
				exportToCSVFile(rs, filename);
				JOptionPane.showMessageDialog(new JFrame(), filename + " saved successfully.");
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(new JFrame(),
						"Problem encountered while querying database.\nPlease try again.");
				e1.printStackTrace();
			}
		}
	}

}

// Word wrap within JTable adapted from - https://stackoverflow.com/a/37768834
class WordWrapCellRenderer extends JTextArea implements TableCellRenderer {
	private static final long serialVersionUID = -6745994514161848770L; // generated serial UID

	WordWrapCellRenderer() {
		setLineWrap(true);
		setWrapStyleWord(true);
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		setText(value.toString());
		setSize(table.getColumnModel().getColumn(column).getWidth(), getPreferredSize().height);
		if (table.getRowHeight(row) != getPreferredSize().height) {
			table.setRowHeight(row, getPreferredSize().height);
		}
		return this;
	}
}
