package assignment;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class MainWindow extends JFrame implements ActionListener {
	private JMenuItem exit;
	private JMenuItem clearDB;
	private JMenuItem populateDB;
	MainWindowContent content;

	public MainWindow() {
		super("MSc JDBC Assignment"); // Window title

		JMenuBar menuBar = new JMenuBar(); // File menu, elements
		JMenu fileMenu = new JMenu("Menu");
		exit = new JMenuItem("Exit");
		populateDB = new JMenuItem("Populate Devices Table With Sample Data");
		clearDB = new JMenuItem("Delete All Devices");
		fileMenu.add(exit);
		fileMenu.add(clearDB);
		fileMenu.add(populateDB);
		menuBar.add(fileMenu);
		setJMenuBar(menuBar);

		exit.addActionListener(this); // Listener for "Exit" option
		clearDB.addActionListener(this);
		populateDB.addActionListener(this);
		
		content = new MainWindowContent("Internet of Things Device Manager");
		getContentPane().add(content); // Populate window with content from MainWindowContent.java

		setSize(1220, 920);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(exit)) {
			System.exit(0);
			this.dispose(); // If the selected option is "Exit", close the window
		}
		if (e.getSource().equals(clearDB)) {
			content.clearDB();
			QueryTableModel qtm = new QueryTableModel();
			try {
				qtm.refreshDevicesTable(DriverManager.getConnection("jdbc:mysql://localhost:3306/IoT_Device_Manager", "root", "admin").createStatement());
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		if (e.getSource().equals(populateDB)) {
			content.populateWithSampleData();
		}
	}

}
