package assignment;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.awt.*;
import java.sql.*;
import java.io.*;
import java.util.*;

@SuppressWarnings("serial")
class QueryTableModel extends AbstractTableModel {
	Vector modelData; // will hold String[] objects
	int colCount;
	String[] headers = {"Name", "Type", "Location", "IP", "MAC", "Software", "Last Updated"};
	String[] roomHeader = {"Room Name"};
	Connection con;
	Statement stmt = null;
	String[] record;
	ResultSet rs = null;

	public QueryTableModel() { // default constructor
		modelData = new Vector();
	}

	public String getColumnName(int i) {
		return headers[i];
	}

	public int getColumnCount() {
		return colCount;
	}

	public int getRowCount() {
		return modelData.size();
	}

	public Object getValueAt(int row, int col) {
		return ((String[]) modelData.elementAt(row))[col];
	}

	public void refreshDevicesTable(Statement s) {
		modelData = new Vector(); // data stored by the table
		stmt = s;
		try {
			// Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("SELECT deviceName as Name, deviceType as Type, deviceLocation as Location, deviceIP as IP, deviceMAC as MAC, softwareVersion as Software, dateLastUpdated as LastUpdated FROM devices");
			ResultSetMetaData meta = rs.getMetaData();

			colCount = meta.getColumnCount(); // number of columns
			
			while (rs.next()) // get all the rows
			{
				record = new String[colCount];

				for (int i = 0; i < colCount; i++) { // for every row, add the data for each column to that row
					record[i] = rs.getString(i + 1);
				}

				modelData.addElement(record); // add the row to the data
			}
			
			fireTableChanged(null); // trigger table changed event
			MainWindowContent.tableOfDevices.getColumnModel().getColumn(0).setWidth(150); // Set width of device name column
			MainWindowContent.tableOfDevices.getColumnModel().getColumn(3).setPreferredWidth(50);
			MainWindowContent.tableOfDevices.getColumnModel().getColumn(5).setPreferredWidth(50);
			MainWindowContent.tableOfDevices.getColumnModel().getColumn(6).setPreferredWidth(50);
		} catch (Exception e) {
			System.out.println("Error with refreshFromDB Method\n" + e);
		}
	} // end refreshFromDB method
	
	public void refreshRoomsTable(Statement s) {
		modelData = new Vector(); // data stored by the table
		stmt = s;
		try {
			rs = stmt.executeQuery("SELECT roomName as 'Room Name' FROM locations");
			ResultSetMetaData meta = rs.getMetaData();
			colCount = meta.getColumnCount();
			headers = new String[colCount];
			headers[0] = "Room Name";
			while (rs.next()) {
				record = new String[colCount];
				for (int i = 0; i < colCount; i++) { // for every row, add the data for each column to that row
					record[i] = rs.getString(i + 1);
				}
				modelData.addElement(record); // add the row to the data
			}
			fireTableChanged(null); // trigger table changed event
		} catch (Exception e) {
			System.out.println("Error with refreshFromDB Method\n" + e.toString());
		}
	}
}