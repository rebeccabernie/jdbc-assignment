CREATE DATABASE IF NOT EXISTS IoT_Device_Manager;
USE IoT_Device_Manager;

DROP TABLE IF EXISTS devices;
DROP TABLE IF EXISTS typesOfDevice;
DROP TABLE IF EXISTS locations;

CREATE TABLE typesOfDevice(
	typeName VARCHAR(50) PRIMARY KEY NOT NULL,
    typeDesc VARCHAR(140)
	);
    
INSERT INTO typesOfDevice VALUES ('SmartHome', 'Smart home devices such as hubs, appliances, assistants.');
INSERT INTO typesOfDevice VALUES ('Kitchen Appliance', 'Smart home devices specific to a kitchen setting, such as fridges.');
INSERT INTO typesOfDevice VALUES ('SmartTV', 'Internet enabled TVs, which allow users to download apps and stream content directly.');
INSERT INTO typesOfDevice VALUES ('Smartphone', 'Mobile phones capable of connecting to the Internet, which allow users to download apps or browse the web.');
INSERT INTO typesOfDevice VALUES ('Smartwatch', 'Internet enabled digital watches, such as an Apple Watch.');
INSERT INTO typesOfDevice VALUES ('PC', 'Includes laptops, notebooks, desktops etc.');
INSERT INTO typesOfDevice VALUES ('Console', 'Games consoles, such as Playstation4, Xbox One, Nintendo Switch.');
INSERT INTO typesOfDevice VALUES ('Tablet', 'Portable tablet devices, such as an Apple iPad.');
INSERT INTO typesOfDevice VALUES ('Other', 'Devices not fitting into the given categories.');

#select * from typesOfDevice;

CREATE TABLE locations(
	roomName VARCHAR(50) NOT NULL,
    PRIMARY KEY (roomName)
	);
    
INSERT INTO locations VALUES ('Kitchen');
INSERT INTO locations VALUES ('Living Room');
INSERT INTO locations VALUES ('Hallway');
INSERT INTO locations VALUES ('Varied');
INSERT INTO locations VALUES ('Bedroom1');
INSERT INTO locations VALUES ('Bedroom2');
INSERT INTO locations VALUES ('Games Room');

#select * from locations;

CREATE TABLE devices(
	deviceName VARCHAR(50) NOT NULL,
	deviceType VARCHAR(20) NOT NULL,
	deviceLocation VARCHAR(20) NOT NULL,
	deviceIP VARCHAR(15),
	deviceMAC VARCHAR(17),
	softwareVersion VARCHAR(10) NOT NULL,
	dateLastUpdated DATE NOT NULL,
    PRIMARY KEY(deviceName),
	FOREIGN KEY (deviceLocation) REFERENCES locations (roomName)

	);

drop procedure if exists populateDatabase;
DELIMITER //
CREATE PROCEDURE populateDatabase()
BEGIN
  #	  						 Device Name	  	 Type			 	Location		 IP				 MAC				  SW Ver		 Date Last Updated
INSERT INTO devices VALUES ('OnePlus3T', 		'Smartphone', 		'Varied',		'10.204.35.34', 'a0:23:ef:2g:12:3f', '5.0.6', 		'2018-09-15');
INSERT INTO devices VALUES ('iPhone7', 			'Smartphone', 		'Varied',		'10.168.24.2', 	'3d:b3:e2:fg:22:3d', '12.0.1', 		'2018-10-09');
INSERT INTO devices VALUES ('Desktop1', 		'PC', 				'Living Room',	'192.168.3.2', 	'0a:32:fe:3g:1f:af', 'W10-1803', 	'2018-09-12');
INSERT INTO devices VALUES ('Samsung S8',		'Smartphone', 		'Varied',		'10.168.14.5', 	'',					 '8.1', 		'2018-09-25');
INSERT INTO devices VALUES ('LGTV', 			'SmartTV', 			'Living Room',	'',			 	'',				     '3.0.9', 		'2018-07-10');
INSERT INTO devices VALUES ('Desktop2', 		'PC', 				'Bedroom2',		'192.168.3.3', 	'a3:f3:ef:eg:1a:3f', 'W10-1802', 	'2018-07-05');
INSERT INTO devices VALUES ('Smarter Coffee 2.0',
												'Kitchen Appliance','Kitchen',		'192.168.22.2', '', 				 '2.4.1', 		'2018-07-26');
INSERT INTO devices VALUES ('OnePlus5', 		'Smartphone', 		'Varied',		'10.204.33.37', 'b0:2e:e3:2a:12:3a', '6.0.3', 		'2018-10-13');
INSERT INTO devices VALUES ('Samsung Family Hub Fridge',
												'Kitchen Appliance','Kitchen',		'192.168.27.6', '', 				 '2.0.3', 		'2018-05-03');
INSERT INTO devices VALUES ('Fitbit', 			'Smartwatch', 		'Varied',		'', 			'',					 '4.0.1', 		'2018-10-15');
INSERT INTO devices VALUES ('Wink Smart Home Hub',
												'SmartHome',		'Kitchen',		'',				'',					 '2.1',			'2018-11-02');
INSERT INTO devices VALUES ('iPad Pro', 		'Tablet',			'Varied',		'',			 	'c3:2e:ef:2g:1a:3f', '11.4.1', 		'2018-07-09');
INSERT INTO devices VALUES ('Philips Hue Hub',	'SmartHome', 		'Living Room',	'', 			'',					 '2.3.1', 		'2018-09-01');
INSERT INTO devices VALUES ('Asus Laptop',		'PC', 				'Varied',		'192.168.42.1', 'ae:2c:e2:2g:13:5c', 'W10-1801', 	'2018-03-12');
INSERT INTO devices VALUES ('Surface Book', 	'PC', 				'Varied',		'10.168.33.2', 	'af:13:7f:cg:1e:3f', 'W10-1803', 	'2018-10-09');
INSERT INTO devices VALUES ('NetGear Arlo Q Security Camera',
												'SmartHome',		'Hallway',		'192.168.19.1',	'',					 '3.2.2',		'2018-06-11');
INSERT INTO devices VALUES ('SamsungTV', 		'SmartTV', 			'Bedroom1',		'192.168.16.5', 'f0:23:ef:2e:f2:3f', '4.3.1', 		'2018-10-01');
INSERT INTO devices VALUES ('LGTV2', 			'SmartTV', 			'Kitchen',		'192.168.15.7', '20:2a:ef:2f:12:3f', '3.0.9', 		'2018-07-10');
INSERT INTO devices VALUES ('SimpliSafe Home Security System',
												'SmartHome',		'Hallway',		'192.168.11.2',	'',					 '4.1.2',		'2018-11-01');
INSERT INTO devices VALUES ('Fitbit2', 			'Smartwatch', 		'Varied',		'',			 	'', 				 '4.0.1', 		'2018-10-15');
INSERT INTO devices VALUES ('August Smart Lock Pro',
												'SmartHome',		'Hallway',		'',				'',					 '2.3',			'2018-10-30');
INSERT INTO devices VALUES ('Nest Smoke&CO2 Detector',
												'SmartHome',		'Kitchen',		'10.168.27.3',  '6b:2a:ef:2g:42:ec', '2.2.1', 		'2018-09-05');
INSERT INTO devices VALUES ('Amazon Echo', 		'SmartHome', 		'Kitchen',		'192.168.24.8', 'f4:23:ef:f2:12:3f', '9.4.1', 		'2018-10-05');
INSERT INTO devices VALUES ('SamsungTV2', 		'SmartTV', 			'Games Room',	'192.168.13.7', '', 				 '2.6.9', 		'2018-09-12');
INSERT INTO devices VALUES ('iRobot Roomba', 	'SmartHome', 		'Varied',		'', 			'1f:23:ef:e1:12:3f', '3.2.3', 		'2018-07-03');
INSERT INTO devices VALUES ('Anova Culinary Precision',
												'Kitchen Appliance','Kitchen',		'192.168.13.2',	'',					 '2.3.1',		'2018-07-24');
INSERT INTO devices VALUES ('XboxOne', 			'Console', 			'Games Room',	'192.168.15.7', '4a:23:ef:2g:12:5a', '2.6.9', 		'2018-10-02');
INSERT INTO devices VALUES ('Playstation4', 	'Console', 			'Games Room',	'192.168.15.7', '',					 '3.4.8', 		'2018-09-30');
INSERT INTO devices VALUES ('Amazon Echo Dot',	'SmartHome', 		'Living Room',	'192.168.23.2', 'a1:23:ef:3f:12:c3', '7.2.1', 		'2018-10-05');
INSERT INTO devices VALUES ('Amazon Echo Dot 2','SmartHome', 		'Games Room',	'192.168.23.3', 'a0:23:ef:2g:12:3f', '7.2.1', 		'2018-10-05');
INSERT INTO devices VALUES ('Crock-Pot Smart Slow Cooker',
												'Kitchen Appliance','Kitchen',		'', 			'', 				 '1.5.9', 		'2018-09-27');
INSERT INTO devices VALUES ('HP Laptop',		'PC', 				'Varied',		'192.168.42.1', '1e:a2:ef:2g:f4:3f', 'W10-1802', 	'2018-07-09');
INSERT INTO devices VALUES ('Nest Smart Thermostat',
												'SmartHome',		'Kitchen',		'', 			'a0:23:ef:2g:12:3f', 				 '2.4.2', 		'2018-08-05');
INSERT INTO devices VALUES ('Awair Smart Air Quality Monitor',
												'SmartHome', 		'Living Room',	'',				'',				 '4.2.5', 		'2018-07-29');
END //
DELIMITER ;

call populateDatabase();
#select * from devices;

# Stored Procedures for Export Selection Buttons
drop procedure if exists exportDevicesOfType;
DELIMITER //
CREATE PROCEDURE exportDevicesOfType
(in exportingChosenType VARCHAR(20))
BEGIN
  SELECT deviceName as Name, deviceType as Type, deviceLocation as Location, deviceIP as IP, deviceMAC as MAC, softwareVersion as Software, dateLastUpdated as LastUpdated FROM devices
	where deviceType = exportingChosenType;
END //
DELIMITER ;

drop procedure if exists exportDevicesInLocation;
DELIMITER //
CREATE PROCEDURE exportDevicesInLocation
(in exportingChosenLoc VARCHAR(20))
BEGIN
  SELECT deviceName as Name, deviceType as Type, deviceLocation as Location, deviceIP as IP, deviceMAC as MAC, softwareVersion as Software, dateLastUpdated as LastUpdated FROM devices
	where deviceLocation = exportingChosenLoc;
END //
DELIMITER ;

drop procedure if exists exportDevicesNotUpdatedSince;
DELIMITER //
CREATE PROCEDURE exportDevicesNotUpdatedSince
(in exportingChosenDate DATE)
BEGIN
  SELECT deviceName as Name, deviceType as Type, deviceLocation as Location, deviceIP as IP, deviceMAC as MAC, softwareVersion as Software, dateLastUpdated as LastUpdated FROM devices
	where dateLastUpdated <= exportingChosenDate;
END //
DELIMITER ;